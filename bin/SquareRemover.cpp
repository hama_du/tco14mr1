bool DEBUG = false;
double TLE = 30.0;

int PARAM0 = 11;
int PARAM1 = 150;
int PARAM2 = 100;

#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <ctime>
#include <stdarg.h>
#include <sys/time.h>
#include <emmintrin.h>

typedef unsigned long long int64;

using namespace std;

const int Conv[4][3] = {
    {-1, 2, 1},
    {-1, 3, 0},
    {-1, 0, 3},
    {-1, 1, 2}
};







const int DX[24] = { 0, 1, 0, -1, 1, 1, -1, -1, 0, 2, 0, -2, -2, -1, 1, 2, 2, 1, -1, -2, -2, 2, -2, 2 };
const int DY[24] = { -1, 0, 1, 0, -1, 1, -1, 1, -2, 0, 2, 0, -1, -2, -2, -1, 1, 2, 2, 1, -2, -2, 2, 2 };

const int QX[4] = { 0, 1, 0, 1 };
const int QY[4] = { 0, 0, 1, 1 };

const int64 CLM = 7;
const int64 DCLM = 63;

const bool WANT_COLUMN[64];

const int N, C;

double _start_time = 0.0;
double _end_time = 0.0;
double _record_time = 0.0;
double _limit_time = 0.0;
int _rlt = 0;
int _rlt2 = 0;

double get_time(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec + tv.tv_usec*1e-6;
}


int64[N] encode(vector<string> board) {
	int64 ret[N];
    for (int i = 0; i < ret.length; i++) {
        for (int j = 0; j < ret.length; j++) {
            long d = board[i][j] - '0';
            ret[i] |= (d << (3 * j));
        }
    }
    return ret;
}

int fastProcess(int score, long[] board, int i, int j, int d) {
    int add = 0;
    int ti = i + DY[d];
    int tj = j + DX[d];
    if (i < ti || (i == ti && j < tj)) {
        add = scoreFast(board, score << 2, i, j, ti, tj);
    } else {
        add = scoreFast(board, score << 2, ti, tj, i, j);
    }
    return add;
}

State process(State state, int i, int j, int d) {
    long[] board = state.board.clone();
    int add = 0;
    if (d == -1) {
        add = scoreSlow(board, state.score << 2);
    } else {
        int ti = i + DY[d];
        int tj = j + DX[d];
        if (i < ti || (i == ti && j < tj)) {
            add = scoreFast(board, state.score << 2, i, j, ti, tj);
            //add += scoreFast(board, (state.score + add) << 2, ti, tj);
        } else {
            add = scoreFast(board, state.score << 2, ti, tj, i, j);
            //add += scoreFast(board, (state.score + add) << 2, i, j);
        }
    }
    return new State(board, state.score + add, state.turn + 1);
}


void init(vector<string> board) {
    N = board.size();
    N2 = N*2;
    N4 = N*4;
    N8 = N*4*sizeof(short);
    NN = N*N;
    for (int i = 1 ; i <= N ; i++) {
        for (int j = 1 ; j <= N ; j++) {
            int pos = i*(N+2)+j;
            Map[pos] = (board[i-1][j-1] == 'L') ? 1 : 2;
            PtoR[pos] = i-1;
            PtoC[pos] = N+j-1;
        }
    }

    Dir[0] = -1;
    Dir[1] = -N-2;
    Dir[2] = N+2;
    Dir[3] = 1;
    NL = (N+2)*(N+2)/64+1;
    NOWSIZE = NL * sizeof(int64);
    ROWSIZE = N2 * sizeof(short);
    for (int i = 0 ; i < N ; i++) {
        RayP[i] = i+1;
        RayP[N+i] = (N+1)*(N+2)+i+1;
        RayP[2*N+i] = (i+1)*(N+2);
        RayP[3*N+i] = (i+1)*(N+2)+N+1;

        RayX[i] = i;
        RayY[i] = -1;
        RayD[i] = 2;
        RayX[i+N] = i;
        RayY[i+N] = N;
        RayD[i+N] = 1;
        RayX[i+2*N] = -1;
        RayY[i+2*N] = i;
        RayD[i+2*N] = 3;
        RayX[i+3*N] = N;
        RayY[i+3*N] = i;
        RayD[i+3*N] = 0;
    }
}


int PC[16384]={0};
void precalc() {
    double val[128][128][4];
    int con[16384] = {0};

    for (int i = 1 ; i <= N ; i++) {
        for (int j = 1 ; j <= N ; j++) {
            int pp = i*(N+2)+j;
            for (int k = 0 ; k < 4 ; k++) {
                int ptn = pp*4+k;
                int np = pp;
                int nd = k;
                int cnt = 0;
                while (true) {
                    if (con[np] != ptn) {
                        cnt++;
                        con[np] = ptn;
                        nd = Conv[nd][Map[np]];
                    }
                    np += Dir[nd];
                    if (!Map[np]) {
                        break;
                    }
                }
                val[i][j][k] = cnt;
            }
        }
    }

    int width = N / 5.5;
    const double drate = 150 * 1.0 / 100;
    for (int i = 1 ; i <= N ; i++) {
        for (int j = 1 ; j <= N ; j++) {
            double sum = 0.0;
            int cnt = 0;
            for (int k = i - width ; k <= i + width ; k++) {
                for (int l = j - width ; l <= j + width ; l++) {
                    if (k <= 0 || k >= N+1 || l <= 0 || l >= N+1) {
                        continue;
                    }
                    int dist = max(abs(i - k), abs(l - j));
                    if (!dist || dist >= width) {
                        continue;
                    }
                    const double div = pow(dist, drate);
                    sum += val[k][l][0] / div;
                    sum += val[k][l][1] / div;
                    sum += val[k][l][2] / div;
                    sum += val[k][l][3] / div;
                    cnt++;
                }
            }
            if (cnt >= 1) {
                PC[i*(N+2)+j] = (int)(sum / cnt);
            }
        }
    }
}


int64 _srnow[200];
short _srrow[200];
int _srdl;
int _pup[512];
int64 _pflg[512][256];
int _bonus = 0;

inline bool stateGet(int64 now[], int npos) {
    const int ni = npos >> 6;
    const int nj = npos & 63;
    return ((now[ni] & (1LLU<<nj)) != 0);
}

void simulate(int b, int ridx, int64 now[], short row[]) {
    double d = 0;
    if (++_rlt2 % 10 == 0) {
        d = get_time();
    }
    memcpy(_srnow, now, NOWSIZE);
    memcpy(_srrow, row, ROWSIZE);
    if (_rlt2 % 10 == 0) {
        _record_time += (get_time() - d);
    }

    int np = RayP[ridx];
    int nd = RayD[ridx];
    _srdl = 0;
    _pup[ridx]=b;
    memset(_pflg[ridx], 0, NOWSIZE);
    _bonus = 0;

    while (true) {
        np += Dir[nd];
        if (!Map[np]) {
            break;
        }
        const int ni = (np >> 6);
        const int nj = np & 63;
        int64 nlj = 1LLU<<nj;
        _pflg[ridx][ni] |= nlj;
        if (!(_srnow[ni] & nlj)) {
            _srnow[ni] |= nlj;
            nd = Conv[nd][Map[np]];
            _srrow[PtoC[np]]--;
            _srrow[PtoR[np]]--;
            _srdl++;
            _bonus = max(_bonus, PC[np]);
        }
    }
}


short _history[512] = {0};

void beam(const int beamWidth) {
    precalc();

    int64 now[2][beamWidth][NL];
    int leftMirrors[2][beamWidth];
    int score[2][beamWidth];
    short history[2][beamWidth][N4];
    short rowcolCnt[2][beamWidth][N2];
    short touched[2][beamWidth];

    for (int i = 0 ; i < 2 ; i++) {
        for (int j = 0 ; j < beamWidth ; j++) {
            touched[i][j] = -1;
        }
    }

    touched[0][0] = 0;
    score[0][1] = -10000;
    score[0][0] = 1;
    leftMirrors[0][0] = NN;
    for (int i = 0 ; i < NL ; i++) {
        now[0][0][i] = 0;
    }
    for (int i = 0 ; i < N4 ; i++) {
        history[0][0][i] = -1;
    }
    for (int i = 0 ; i < N2 ; i++) {
        rowcolCnt[0][0][i] = N;
    }

    int64 difmap[NL];

    int64 rejected = 0;
    int64 simulated = 0;
    int64 added = 0;
    int64 cnt = 0;
    int64 hashHit = 0;

    int minLeft = NN;
    const int THS = N*N*3/5;
    const int THS2 = N*N*PARAM1/100;
    const int N05_3 = pow(N, 0.5) * 3 - 5 + PARAM0;

    const int rowsize = ROWSIZE;
    const int nowsize = NOWSIZE;
    for (int cur = 0 ; cur < N4 ; cur++) {
        const int fr = (cur & 1);
        const int to = 1 - fr;
        int tbidx = 0;
        set<int64> nextHash;
        priority_queue<pair<int,int>, vector<pair<int,int> >, greater<pair<int,int> > > q;
        for (int i = 0 ; i < NL ; i++) {
            difmap[i] = 0;
        }
        const int currentBeamWidth = (get_time() >= _limit_time) ? 4 : beamWidth;
        const double adjustRate = (minLeft <= THS2) ? 2 : 1;
        for (int b = 0 ; b < currentBeamWidth ; b++) {
            if (touched[fr][b] != cur) {
                break;
            }
            if (b >= 1) {
                for (int i = 0 ; i < NL ; i++) {
                    difmap[i] = now[fr][b-1][i] ^ now[fr][b][i];
                }
            }
            for (int r = 0 ; r < N4 ; r++) {
                if (RayX[r] == -1 || RayX[r] == N) {
                    if (!rowcolCnt[fr][b][RayY[r]]) {
                        continue;
                    }
                } else {
                    if (!rowcolCnt[fr][b][N+RayX[r]]) {
                        continue;
                    }
                }

                const int fpos = RayP[r]+Dir[RayD[r]];
                if (!stateGet(now[fr][b], fpos)) {
                    const int td = Conv[RayD[r]][Map[fpos]];
                    const int tpos = fpos + Dir[td];
                    if (!Map[tpos]) {
                        continue;
                    }
                    if (!stateGet(now[fr][b], tpos)) {
                        if (Map[fpos] != Map[tpos]) {
                            continue;
                        }
                    }
                }

                if (_pup[r] == b-1) {
                    bool cov = false;
                    for (int i = 0 ; i < NL ; i++) {
                        if (_pflg[r][i] & difmap[i]) {
                            cov = true;
                            break;
                        }
                    }
                    if (!cov) {
                        _pup[r] = b;
                        continue;
                    }
                }

                simulate(b, r, now[fr][b], rowcolCnt[fr][b]);
                const int tl = leftMirrors[fr][b] - _srdl;
                minLeft = min(minLeft, tl);

                if (!tl) {
                    memcpy(_history, history[fr][b], N8);
                    _history[cur] = r;
                    if (DEBUG) {
                        cerr << "r/s/h/a" << endl;
                        cerr << rejected << "/" << simulated << "/" << hashHit << "/" << added << endl;
                        cerr << "end : turn #" << cur << endl;
                    }
                    return;
                }

                int ts = NN - tl;
                int tc = 0;
                const int thdv = 10;
                for (int i = 0 ; i < N2 ; i++) {
                    if (1 <= _srrow[i] && _srrow[i] <= N05_3) {
                        tc += (N05_3 - _srrow[i]);
                    }
                }
                const int penalty = tc / thdv;
                ts -= penalty;
                if (minLeft >= THS) {
                    ts += _bonus;
                } else {
                    ts -= penalty * adjustRate;
                }

                int willGo = tbidx;
                if (tbidx < beamWidth) {
                    int64 hashValue = 0;
                    for (int i = 0 ; i < NL ; i++) {
                        hashValue *= MOD;
                        hashValue += _srnow[i];
                    }
                    // same hash value -> probably same state. skip
                    if (nextHash.find(hashValue) != nextHash.end()) {
                        continue;
                    }
                    nextHash.insert(hashValue);
                    q.push(make_pair(ts,tbidx));
                    tbidx++;
                } else {
                    pair<int,int> low = q.top();
                    if (low.first < ts) {
                        int64 hashValue = 0;
                        for (int i = 0 ; i < NL ; i++) {
                            hashValue *= MOD;
                            hashValue += _srnow[i];
                        }

                        // same hash value -> probably same state. skip
                        if (nextHash.find(hashValue) != nextHash.end()) {
                            continue;
                        }
                        nextHash.insert(hashValue);
                        q.pop();
                        q.push(make_pair(ts,low.second));
                        willGo = low.second;
                    } else {
                        willGo = -1;
                    }
                }
                if (willGo != -1) {
                    leftMirrors[to][willGo] = tl;
                    score[to][willGo] = ts;

                    // double d = 0;
                    // if (++_rlt % 10 == 0) {
                    //     d = get_time();
                    // }

                    memcpy(now[to][willGo], _srnow, nowsize);
                    memcpy(rowcolCnt[to][willGo], _srrow, rowsize);
                    memcpy(history[to][willGo], history[fr][b], N8);

                    // if (_rlt % 10 == 0) {
                    //     _record_time += (get_time() - d);
                    // }

                    history[to][willGo][cur] = r;
                    touched[to][willGo] = cur+1;
                }
            }
        }
    }
}


vector<int> output() {
    vector<int> ans;
    for (int i = 0 ; i < N4 ; i++) {
        if (_history[i] == -1) {
            break;
        }
        ans.push_back(RayY[_history[i]]);
        ans.push_back(RayX[_history[i]]);
    }
    return ans;
}

class FragileMirrors
{
public:
    vector<int> destroy(vector<string> board) {
        _start_time = get_time();
        _limit_time = _start_time + TLE * 0.95;
        init(board);

        const int bw = (int)(180000000LLU / pow(N, 3.0));
        if (DEBUG) {
            cerr << "bw : " << bw << endl;
        }
        beam(bw);
        return output();
    }
};

int main(int argc,char** args)
{
    if (argc >= 2) {
        DEBUG = atoi(args[1]) >= 1;
    }
    if (argc >= 3) {
        PARAM0 = atoi(args[2]);
    }
    if (argc >= 4) {
        PARAM1 = atoi(args[3]);
    }
    if (argc >= 5) {
        PARAM2 = atoi(args[4]);
    }

    int n;
    cin >> n;

    vector<string> board;
    for (int i=0;i<n;i++) {
        string s;
        cin >> s;
        board.push_back(s);
    }

    FragileMirrors fm;
    vector<int> ret=fm.destroy(board);
    _end_time = get_time();
    if (DEBUG) {
        cerr << "n=" << n << endl;
        cerr << "p1,p2,p3" << endl << PARAM0 << "," << PARAM1 << "," << PARAM2 << endl;
        cerr << "end in " << (_end_time - _start_time) << " seconds" << endl;
        cerr << "rec : " << _record_time << endl;
    }

    int retsz = ret.size();
    cout << retsz << endl;
    for (int i = 0 ; i < retsz ; i++) {
        cout << ret[i] << endl;
    }
    return 0;
}

