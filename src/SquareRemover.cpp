bool DEBUG = false;
double TLE = 30.0;

long _xtime = 30000;
int _maxTurn = 10000;
bool _debug = false;

#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <ctime>
#include <stdarg.h>
#include <sys/time.h>
#include <emmintrin.h>

typedef long long int int64;

using namespace std;

const int DX[24] = { 0, 1, 0, -1, 1, 1, -1, -1, 0, 2, 0, -2, -2, -1, 1, 2, 2, 1, -1, -2, -2, 2, -2, 2 };
const int DY[24] = { -1, 0, 1, 0, -1, 1, -1, 1, -2, 0, 2, 0, -1, -2, -2, -1, 1, 2, 2, 1, -2, -2, 2, 2 };

const int QX[4] = { 0, 1, 0, 1 };
const int QY[4] = { 0, 0, 1, 1 };

const int64 CLM = 7;
const int64 DCLM = 63;
size_t ROWSIZE = sizeof(int64);

bool WANT_COLUMN[64];
int N, C;

double _start_time = 0.0;
double _end_time = 0.0;
double _record_time = 0.0;
double _limit_time = 0.0;
int _rlt = 0;
int _rlt2 = 0;


const int MAX_N = 16;
const int MAX_C = 6;

const int BUFFER_SIZE = 200000;
int64 BUFFER[BUFFER_SIZE] = {0};
int64 BUFFER_HEAD = 0;

double get_time(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec + tv.tv_usec*1e-6;
}



class State
{
public:
    int64 board[MAX_N];
    int score;
    vector<int> operations;
    int parentTurn;
    int parentIndex;
    int subScore;

    State() {

    }

    State(int64 b[MAX_N], int s) {
        memcpy(board, b, ROWSIZE);
        score = s;
    }

    State(int64 b[MAX_N], int s, vector<int> op, int pt, int pi) {
        memcpy(board, b, ROWSIZE);
        score = s;
        operations = op;
        parentTurn = pt;
        parentIndex = pi;
    }
};


void swap(int64 board[MAX_N], int i, int j, int d) {
    int ti = i + DY[d];
    int tj = j + DX[d];
    int j3 = 3*j;
    int tj3 = 3*tj;
    int64 c1 = (board[i] >> j3) & CLM;
    int64 c2 = (board[ti] >> tj3) & CLM;
    board[i] -= c1 << j3;
    board[ti] -= c2 << tj3;
    board[i] |= c2 << j3;
    board[ti] |= c1 << tj3;
}


int scoreSlow(int64 encoded[MAX_N], int bidx, int fi) {
    int plus = 0;
    bool upd = true;
    while (true) {
        upd = false;
        for (int i = max(0, fi-1); i < N - 1; i++) {
            for (int j = 0; j < N - 1; j++) {
                int row1 = (int) ((encoded[i] >> (3 * j)) & DCLM);
                int row2 = (int) ((encoded[i + 1] >> (3 * j)) & DCLM);
                if (WANT_COLUMN[row1] && row1 == row2) {
                    plus++;
                    upd = true;
                    int64 reg = ~(DCLM << (3 * j));
                    int64 up = ((BUFFER[bidx]) | ((BUFFER[bidx + 1]) << 3)) << (3 * j);
                    int64 dw = ((BUFFER[bidx + 2]) | ((BUFFER[bidx + 3]) << 3)) << (3 * j);
                    encoded[i] = (encoded[i] & reg) | up;
                    encoded[i + 1] = (encoded[i + 1] & reg) | dw;
                    bidx += 4;
                    fi = i;
                    goto sch;
                }
            }
        }
    sch:
        if (!upd) {
            break;
        }
    }
    return plus;
}

/**
 * count score (it's slow)
 *
 * @param encoded
 * @return
 */
int scoreSlow(int64 encoded[MAX_N], int bidx) {
    return scoreSlow(encoded, bidx, 0);
}


/**
 * count score (only on interested point. fast)
 *
 * @param encoded
 * @param bidx
 * @param ci
 * @param cj
 */
int scoreFast(int64 encoded[MAX_N], int bidx, int ci, int cj, int di, int dj) {
    const int imax = (ci == di) ? min(N-2, ci) : min(N-2, ci+1);
    const int jmax = (cj == dj) ? min(N-2, cj) : min(N-2, cj+1);
    for (int i = max(0, ci - 1); i <= imax; i++) {
        for (int j = max(0, cj - 1); j <= jmax ; j++) {
            int row1 = (int) ((encoded[i] >> (3 * j)) & DCLM);
            int row2 = (int) ((encoded[i + 1] >> (3 * j)) & DCLM);
            if (WANT_COLUMN[row1] && row1 == row2) {
                int64 reg = ~(DCLM << (3 * j));
                int64 up = ((BUFFER[bidx]) | ((BUFFER[bidx + 1]) << 3)) << (3 * j);
                int64 dw = ((BUFFER[bidx + 2]) | ((BUFFER[bidx + 3]) << 3)) << (3 * j);
                encoded[i] = (encoded[i] & reg) | up;
                encoded[i + 1] = (encoded[i + 1] & reg) | dw;
                bidx += 4;
                return 1 + scoreSlow(encoded, bidx, i);
            }
        }
    }
    return 0;
}



/**
 * process state fast
 *
 * @param score
 * @param board
 * @param i
 * @param j
 * @param d
 * @return
 */
int fastProcess(int score, int64 board[MAX_N], int i, int j, int d) {
    int add = 0;
    int ti = i + DY[d];
    int tj = j + DX[d];
    if (i < ti || (i == ti && j < tj)) {
        add = scoreFast(board, score << 2, i, j, ti, tj);
    } else {
        add = scoreFast(board, score << 2, ti, tj, i, j);
    }
    return add;
}



int findPathScore = 0;
int findPathOP[128];
int findPathOPIdx = 0;
int collectFourFixed[256];
bool findPath(int fi, int fj, int col, int64 brd[MAX_N]) {
    // same
    if (((brd[fi] >> (3 * fj)) & 7) == col) {
        return true;
    }

    //
    //  1
    // 1*1
    //  1
    //
    for (int d = 0 ; d < 4 ; d++) {
        int ti = fi + DY[d];
        int tj = fj + DX[d];
        if (ti < 0 || tj < 0 || ti >= N || tj >= N || collectFourFixed[(ti<<4)+tj]) {
            continue;
        }
        if (((brd[ti] >> (3 * tj)) & 7) == col) {
            findPathOP[findPathOPIdx++] = (fi<<8)|(fj<<4)|d;
            swap(brd, fi, fj, d);
            return true;
        }
    }

    //
    // 2-2
    // -*-
    // 2-2
    //
    for (int d = 4 ; d < 8 ; d++) {
        int ti = fi + DY[d];
        int tj = fj + DX[d];
        if (ti < 0 || tj < 0 || ti >= N || tj >= N || collectFourFixed[(ti<<4)+tj]) {
            continue;
        }
        if (((brd[ti] >> (3 * tj)) & 7) == col) {
            // which way to go?
            //  y@
            //  *x
            int codex = (fi<<4)+tj;
            int codey = (ti<<4)+fj;
            if (collectFourFixed[codex] && collectFourFixed[codey]) {
                continue;
            }
            int dx = ((d-4)/2)*2+1;
            int dy = (d%2)*2;
            bool goX = false;
            if (collectFourFixed[codey]) {
                // x is okay
                // -@
                // **
                goX = true;
            } else if (collectFourFixed[codex]) {
                // y is okay
                // *@
                // *-
            } else {
                // both is okay
                //  ??
                // y@?
                // *x
                goX = true;
            }
            if (goX) {
                findPathOP[findPathOPIdx++] = (fi<<8)|(tj<<4)|dy;
                swap(brd, fi, tj, dy);
                findPathOP[findPathOPIdx++] = (fi<<8)|(fj<<4)|dx;
                swap(brd, fi, fj, dx);
            } else {
                findPathOP[findPathOPIdx++] = (ti<<8)|(fj<<4)|dx;
                swap(brd, ti, fj, dx);
                findPathOP[findPathOPIdx++] = (fi<<8)|(fj<<4)|dy;
                swap(brd, fi, fj, dy);
            }
            return true;
        }
    }


    //  3
    // ---
    //3-*-3
    // ---
    //  3
    for (int d = 8 ; d < 12 ; d++) {
        int ti = fi + DY[d];
        int tj = fj + DX[d];
        if (ti < 0 || tj < 0 || ti >= N || tj >= N || collectFourFixed[(ti<<4)+tj]) {
            continue;
        }
        if (((brd[ti] >> (3 * tj)) & 7) == col) {
            int ci = (fi+ti)/2;
            int cj = (fj+tj)/2;
            if (collectFourFixed[(ci<<4)+cj]) {
                continue;
            }
            findPathOP[findPathOPIdx++] = (ci<<8)|(cj<<4)|(d-8);
            swap(brd, ci, cj, d-8);
            findPathOP[findPathOPIdx++] = (fi<<8)|(fj<<4)|(d-8);
            swap(brd, fi, fj, d-8);
            return true;
        }
    }
    return false;
}


int collectFourBestScore;
int64 collectFourBestBoard[MAX_N];
void collectFour(int i, int j, int col, int64 brd[MAX_N], int score) {
    memset(collectFourFixed, 0, 256 * sizeof(int));
    findPathOPIdx = 0;
    collectFourBestScore = -1;
    int prm[4] = {0, 0, 0, 0};
    int ph = 0;
    int pt = 3;
    for (int q = 0; q < 4; q++) {
        int fi = i + QY[q];
        int fj = j + QX[q];
        if (((brd[fi] >> (3 * fj)) & 7) == col) {
            prm[ph++] = q;
        } else {
            prm[pt--] = q;
        }
    }

    bool isGood = true;
    int64 cpy[MAX_N];
    memcpy(cpy, brd, ROWSIZE);
    for (int q = 0; q < 4; q++) {
        int fi = i + QY[prm[q]];
        int fj = j + QX[prm[q]];
        bool isok = findPath(fi, fj, col, cpy);
        if (!isok) {
            isGood = false;
            break;
        }
        collectFourFixed[(fi<<4)+fj] = 1;
    }
    if (isGood) {
        memcpy(collectFourBestBoard, brd, ROWSIZE);
        int bscore = score;
        for (int o = 0 ; o < findPathOPIdx ; o++) {
            int k = findPathOP[o];
            int fi = k>>8;
            int fj = (k>>4)&15;
            int d = k&15;
            swap(collectFourBestBoard, fi, fj, d);
            score += fastProcess(score, collectFourBestBoard, fi, fj, d);
        }
        collectFourBestScore = score - bscore;
    }
}




vector<int> output(vector<int> result) {
    vector<int> ret;
    for (int i = 0 ; i < 10000 ; i++) {
        if (i < result.size()) {
            int k = result[i];
            ret.push_back(k>>8);
            ret.push_back((k>>4)&15);
            ret.push_back(k&15);
        } else {
            ret.push_back(1);
            ret.push_back(1);
            ret.push_back(1);
        }
    }
    return ret;
}

void initialize(int colors, vector<string> board, int startSeed, int64 ret[MAX_N]) {
    N = board.size();
    C = colors;
    ROWSIZE *= N;
    for (int i = 0 ; i < 64 ; i++) {
        WANT_COLUMN[i] = false;
    }
    WANT_COLUMN[0] = true;
    WANT_COLUMN[1 | (1 << 3)] = true;
    WANT_COLUMN[2 | (2 << 3)] = true;
    WANT_COLUMN[3 | (3 << 3)] = true;
    WANT_COLUMN[4 | (4 << 3)] = true;
    WANT_COLUMN[5 | (5 << 3)] = true;
    BUFFER[0] = (int)(startSeed % C);
    BUFFER_HEAD = startSeed;
    for (int i = 1; i < BUFFER_SIZE; i++) {
        BUFFER_HEAD = (BUFFER_HEAD * 48271) % (2147483647L);
        BUFFER[i] = (int)(BUFFER_HEAD % C);
    }
    if (DEBUG) {
      cerr << N << "," << C << endl;
    }
    for (int i = 0 ; i < N ; i++) {
        for (int j = 0 ; j < N ; j++) {
            int64 z = board[i][j] - '0';
            ret[i] |= z<<(3LL*j);
        }
    }
}



vector<int> beam(int64 initialBoard[MAX_N]) {
    vector<int> result;
    int BW = ((int)(sqrt(8000.0/(pow(N-1, 1.7)*(pow(C, -1.2)))))+1);
    const int ROT = 16;

    vector<int> emptyOperation;
    vector<State> beam[10020];
    int initialScore = scoreSlow(initialBoard, 0);
    State initialState = State(initialBoard, initialScore, emptyOperation, -1, -1);
    beam[0].push_back(initialState);
    int beamIdx[10020];
    for (int i = 0 ; i < 10020 ; i++) {
        beamIdx[i] = 0;
    }
    beamIdx[0] = 1;

    int sum[MAX_C][MAX_N+1][MAX_N+1];
    for (int c = 0 ; c <= MAX_C ; c++) {
      for (int i = 0 ; i <= MAX_N ; i++) {
        for (int j = 0 ; j <= MAX_N ; j++) {
          sum[c][i][j] = 0;
        }
      }
    }


    for (int T = 0 ; T < _maxTurn ; T++) {
        for (int B = 0 ; B < beamIdx[T] ; B++) {
            State* now = &(beam[T][B]);
            int nxcol[MAX_C] = {0};
            int bidx = (now->score)<<2;
            while (true) {
                nxcol[BUFFER[bidx]]++;
                nxcol[BUFFER[bidx+1]]++;
                nxcol[BUFFER[bidx+2]]++;
                nxcol[BUFFER[bidx+3]]++;
                int c = BUFFER[bidx];
                if (nxcol[c] < 4) {
                  break;
                }
                nxcol[c] = 0;
                bidx += 4;
            }

            double _z = get_time();

            int nextBidx = bidx;
            for (int i = 0 ; i < N ; i++) {
                for (int j = 0 ; j < N ; j++) {
                    int co = (((now->board[i]) >> (3 * j)) & 7);
                    for (int c = 0 ; c < C ; c++) {
                        sum[c][i+1][j+1] = sum[c][i+1][j] + sum[c][i][j+1] - sum[c][i][j];
                        if (co == c) {
                            sum[c][i+1][j+1] += 1;
                        }
                    }
                }
            }

            int subScore = 0;
            for (int i = 0 ; i < N-1 ; i++) {
                for (int j = 0 ; j < N-1 ; j++) {
                    int co[MAX_C] = {0};
                    for (int q = 0 ; q < 4 ; q++) {
                        int c = (int)((now->board[i+QY[q]] >> (3 * (j+QX[q]))) & 7);
                        co[c]++;
                    }
                    sort(co, co+MAX_C);

                    int add = 0;
                    if (co[MAX_C-1] == 2) {
                        add += 2;
                        if (co[MAX_C-2] == 2) {
                            add += 2;
                        }
                    } else if (co[MAX_C-1] == 3) {
                        add += 6;
                    }
                    subScore += add;
                }
            }
            now->subScore = subScore;

            int candidates = 0;
            int scores[256] = {0};
            for (int i = 0; i < N-1 ; i++) {
                for (int j = 0; j < N-1; j++) {
                    int maxColorNum = 0;
                    int colors[MAX_C] = {0};
                    for (int q = 0; q < 4; q++) {
                        int ti = i + QY[q];
                        int tj = j + QX[q];
                        int c = (int)((now->board[ti] >> (3 * tj)) & 7);
                        colors[c]++;
                        maxColorNum = max(maxColorNum, colors[c]);
                    }
                    if (maxColorNum <= 0) {
                        continue;
                    }

                    int mul = 0;
                    for (int q = 0 ; q < 4 ; q++) {
                        int ei = i + QY[q];
                        int ej = j + QX[q];
                        int c = (int)BUFFER[nextBidx+q];
                        int del = (sum[c][i+2][j+2] - sum[c][i][j+2] - sum[c][i+2][j] + sum[c][i][j]);
                        int fi = max(0, ei-1);
                        int fj = max(0, ej-1);
                        int ti = min(ei+2, N);
                        int tj = min(ej+2, N);
                        int add = (sum[c][ti][tj] - sum[c][fi][tj] - sum[c][ti][fj] + sum[c][fi][fj]);
                        mul += (add - del);
                    }
                    int sc = mul;
                    scores[(i << 4) + j] = ((sc << 16) | (i << 4)) | j;
                    candidates++;
                }
            }

            sort(scores, scores+256);


            _record_time += get_time() - _z;

            for (int d = 0; d < min(candidates/2,36) ; d++) {
                if (scores[255-d] == 0) {
                    break;
                }
                int pos = scores[255-d] & ((1<<16)-1);
                int i = pos >> 4;
                int j = pos & 15;
                int colors[MAX_C] = {0};
                int maxColorNum = 0;
                for (int q = 0; q < 4; q++) {
                    int ti = i + QY[q];
                    int tj = j + QX[q];
                    int c = (int)((now->board[ti] >> (3 * tj)) & 7);
                    colors[c]++;
                    maxColorNum = max(maxColorNum, colors[c]);
                }
                for (int col = 0; col < C; col++) {
                    if (colors[col] < maxColorNum)  {
                        continue;
                    }
                    collectFour(i, j, col, now->board, now->score);
                    if (collectFourBestScore < 0) {
                        continue;
                    }
                    int tT = (T + findPathOPIdx);

                    int ts = collectFourBestScore + now->score;
                    int minScore = 1<<16;
                    int minSubScore = 1<<16;
                    int minScoreIdx = -1;

                    bool foundSameWorse = false;
                    for (int k = 0 ; k < beamIdx[tT] ; k++) {
                        if (beam[tT][k].score <= ts) {
                            bool isSame = true;
                            for (int f = 0 ; f < N ; f++) {
                                if (beam[tT][k].board[f] != collectFourBestBoard[f]) {
                                    isSame = false;
                                    break;
                                }
                            }
                            if (isSame) {
                                foundSameWorse = true;
                                minScoreIdx = k;
                            }
                        }
                    }
                    if (!foundSameWorse) {
                        if (beamIdx[tT] < BW) {
                          minScoreIdx = beamIdx[tT];
                        } else {
                          for (int k = 0 ; k < beamIdx[tT] ; k++) {
                              int DSi = (ts - beam[tT][k].score);
                              int DSSi = (subScore - beam[tT][k].subScore);
                              int TH = 1000*C/N/N;
                              if (T + _maxTurn / 20 >= _maxTurn) {
                                  TH = 10000;
                              }
                              if (DSi > 0 || ((DSSi + DSi * TH) > 0)) {
                                  int _DSi = (minScore - beam[tT][k].score);
                                  int _DSSi = (minSubScore - beam[tT][k].subScore);
                                  if (_DSi > 0 || ((_DSSi + _DSi * TH) > 0)) {
                                      minScore = beam[tT][k].score;
                                      minSubScore = beam[tT][k].subScore;
                                      minScoreIdx = k;
                                  }
                              }
                          }
                       }
                    }
                    if (minScoreIdx >= 0) {
                        vector<int> ops;
                        for (int o = 0 ; o < findPathOPIdx ; o++) {
                            ops.push_back(findPathOP[o]);
                        }
                        if (beamIdx[tT] < BW) {
                          State newState = State(collectFourBestBoard, ts, ops, T, B);
                          newState.subScore = now->subScore;
                          beam[tT].push_back(newState);
                          beamIdx[tT]++;
                        } else {
                          memcpy(beam[tT][minScoreIdx].board, collectFourBestBoard, ROWSIZE);
                          beam[tT][minScoreIdx].score = ts;
                          beam[tT][minScoreIdx].operations = ops;
                          beam[tT][minScoreIdx].parentTurn = T;
                          beam[tT][minScoreIdx].parentIndex = B;
                          beam[tT][minScoreIdx].subScore = now->subScore;
                        }
                    }
                }
            }
        }
    }

    int score = -1;
    int nowTurn = _maxTurn;
    int nowIdx = -1;
    State *bestState;
    for (int t = _maxTurn ; t <= _maxTurn ; t++) {
        for (int l = 0 ; l < beamIdx[t] ; l++) {
            if (score < beam[t][l].score) {
                score = beam[t][l].score;
                nowIdx = l;
            }
        }
    }

    vector<int> ops;
    vector<int> revop;
    while (true) {
        int sz = beam[nowTurn][nowIdx].operations.size();
        for (int i = sz-1 ; i >= 0 ; i--) {
          revop.push_back(beam[nowTurn][nowIdx].operations[i]);
        }
        if (nowTurn == 0) {
          break;
        }
        int pt = beam[nowTurn][nowIdx].parentTurn;
        int pi = beam[nowTurn][nowIdx].parentIndex;
        nowTurn = pt;
        nowIdx = pi;
    }
    int rs = revop.size();
    if (rs > 10000) {
      rs = 10000;
    }
    for (int i = rs-1 ; i >= 0 ; i--) {
        ops.push_back(revop[i]);
    }
    return ops;
}


class SquareRemover
{
public:

    vector<int> playIt(int colors, vector<string> board, int startSeed) {
        _start_time = get_time();
        _limit_time = _start_time + (_xtime * 0.95) / 1000;
        int64 initialBoard[MAX_N];
        for (int i = 0 ; i < MAX_N ; i++) {
          initialBoard[i] = 0;
        }
        initialize(colors, board, startSeed, initialBoard);
        vector<int> result = beam(initialBoard);

        return output(result);
    }
};


int main(int argc,char** args)
{
    if (argc >= 2) {
        _xtime = atoi(args[1]);
    }
    if (argc >= 3) {
        _maxTurn = atoi(args[2]);
    }
    if (argc >= 4) {
        DEBUG = atoi(args[3]) >= 1;
    }

    int c, n;
    cin >> c;
    cin >> n;
    vector<string> board;
    for (int i=0;i<n;i++) {
        string s;
        cin >> s;
        board.push_back(s);
    }
    int startSeed;
    cin >> startSeed;


    SquareRemover sr;
    vector<int> ret=sr.playIt(c, board, startSeed);
    _end_time = get_time();
    if (DEBUG) {
        cerr << "n=" << n << endl;
        cerr << "end in " << (_end_time - _start_time) << " seconds" << endl;
        cerr << "rec : " << _record_time << endl;
    }

    int retsz = ret.size();
    for (int i = 0 ; i < retsz ; i++) {
        cout << ret[i] << endl;
    }
    return 0;
}
