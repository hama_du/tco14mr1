import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class SquareRemover {
    // arg[0]
    static long _xtime = 30000;

    // arg[1]
    static int _maxTurn = 10000;

    // arg[2]
    static boolean _debug = false;

    static long _start_time = -1, _end_time = -1;

    static Map<String,Long> _record_timer = new HashMap<String,Long>();

    static Map<String,Long> _recorder = new HashMap<String,Long>();

    static long _step = 0;

    static final double EPS = 1e-8;

    static int C;

    static int N;

    static final Random _rnd = new Random(9999991);

    static final boolean[] WANT_COLUMN = new boolean[64];

    static final long[] BUFFER = new long[200000];
    static long BUFFER_HEAD = 0;

    //  kd8el
    //  c604f
    //  b3*19
    //  j725g
    //  miahn
    static final int[] DX = { 0, 1, 0, -1, 1, 1, -1, -1, 0, 2, 0, -2, -2, -1, 1, 2, 2, 1, -1, -2, -2, 2, -2, 2 };
    static final int[] DY = { -1, 0, 1, 0, -1, 1, -1, 1, -2, 0, 2, 0, -1, -2, -2, -1, 1, 2, 2, 1, -2, -2, 2, 2 };

    // 01
    // 23
    static final int[] QX = { 0, 1, 0, 1 };
    static final int[] QY = { 0, 0, 1, 1 };

    static final long CLM = 7;
    static final long DCLM = 63;

    /**
     * encode the board to long[]
     *
     * @param board
     * @return
     */
    public static long[] encode(String[] board) {
        long[] ret = new long[board.length];
        for (int i = 0; i < ret.length; i++) {
            for (int j = 0; j < ret.length; j++) {
                long d = board[i].charAt(j) - '0';
                ret[i] |= (d << (3 * j));
            }
        }
        return ret;
    }

    /**
     * decodes the board
     *
     * @param code
     * @return
     */
    public String[] decode(long[] code) {
        int n = code.length;
        String[] ret = new String[n];
        for (int i = 0; i < n; i++) {
            ret[i] = "";
            for (int j = 0; j < n; j++) {
                long c = (code[i] >> (j * 3)) & 7;
                ret[i] += (char) ('0' + c);
            }
        }
        return ret;
    }


    /**
     * process state fast
     *
     * @param score
     * @param board
     * @param i
     * @param j
     * @param d
     * @return
     */
    public int fastProcess(int score, long[] board, int i, int j, int d) {
        int add = 0;
        int ti = i + DY[d];
        int tj = j + DX[d];
        if (i < ti || (i == ti && j < tj)) {
            add = scoreFast(board, score << 2, i, j, ti, tj);
        } else {
            add = scoreFast(board, score << 2, ti, tj, i, j);
        }
        return add;
    }


    /**
     * compute next state
     *
     * @param state
     * @return
     */
    public State process(State state, int i, int j, int d) {
        long[] board = state.board.clone();
        int add = 0;
        if (d == -1) {
            add = scoreSlow(board, state.score << 2);
        } else {
            int ti = i + DY[d];
            int tj = j + DX[d];
            if (i < ti || (i == ti && j < tj)) {
                add = scoreFast(board, state.score << 2, i, j, ti, tj);
                //add += scoreFast(board, (state.score + add) << 2, ti, tj);
            } else {
                add = scoreFast(board, state.score << 2, ti, tj, i, j);
                //add += scoreFast(board, (state.score + add) << 2, i, j);
            }
        }
        return new State(board, state.score + add, state.turn + 1);
    }

    /**
     * swap adjacent cell
     *
     * @param board
     * @param i
     * @param j
     * @param d
     */
    public static void swap(long[] board, int i, int j, int d) {
        int ti = i + DY[d];
        int tj = j + DX[d];
        int j3 = 3*j;
        int tj3 = 3*tj;
        long c1 = (board[i] >> j3) & CLM;
        long c2 = (board[ti] >> tj3) & CLM;
        board[i] -= c1 << j3;
        board[ti] -= c2 << tj3;
        board[i] |= c2 << j3;
        board[ti] |= c1 << tj3;
    }

    /**
     * count score (it's slow)
     *
     * @param encoded
     * @return
     */
    public int scoreSlow(long[] encoded, int bidx) {
    	return scoreSlow(encoded, bidx, 0);
    }
    public int scoreSlow(long[] encoded, int bidx, int fi) {
        int plus = 0;
        int n = encoded.length;
        boolean upd = true;
        while (upd) {
            upd = false;
            sch: for (int i = Math.max(0, fi-1); i < n - 1; i++) {
                for (int j = 0; j < n - 1; j++) {
                    int row1 = (int) ((encoded[i] >> (3 * j)) & DCLM);
                    int row2 = (int) ((encoded[i + 1] >> (3 * j)) & DCLM);
                    if (WANT_COLUMN[row1] && row1 == row2) {
                        plus++;
                        upd = true;
                        long reg = ~(DCLM << (3 * j));
                        long up = ((BUFFER[bidx]) | ((BUFFER[bidx + 1]) << 3)) << (3 * j);
                        long dw = ((BUFFER[bidx + 2]) | ((BUFFER[bidx + 3]) << 3)) << (3 * j);
                        encoded[i] = (encoded[i] & reg) | up;
                        encoded[i + 1] = (encoded[i + 1] & reg) | dw;
                        bidx += 4;
                        fi = i;
                        break sch;
                    }
                }
            }
        }
        return plus;
    }

    /**
     * count score (only on interested point. fast)
     *
     * @param encoded
     * @param bidx
     * @param ci
     * @param cj
     */
    public int scoreFast(long[] encoded, int bidx, int ci, int cj, int di, int dj) {
        // ++.
        // +*.
        // ...
    	final int imax = (ci == di) ? Math.min(N-2, ci) : Math.min(N-2, ci+1);
    	final int jmax = (cj == dj) ? Math.min(N-2, cj) : Math.min(N-2, cj+1);
        for (int i = Math.max(0, ci - 1); i <= imax; i++) {
            for (int j = Math.max(0, cj - 1); j <= jmax ; j++) {
                int row1 = (int) ((encoded[i] >> (3 * j)) & DCLM);
                int row2 = (int) ((encoded[i + 1] >> (3 * j)) & DCLM);
                if (WANT_COLUMN[row1] && row1 == row2) {
                    long reg = ~(DCLM << (3 * j));
                    long up = ((BUFFER[bidx]) | ((BUFFER[bidx + 1]) << 3)) << (3 * j);
                    long dw = ((BUFFER[bidx + 2]) | ((BUFFER[bidx + 3]) << 3)) << (3 * j);
                    encoded[i] = (encoded[i] & reg) | up;
                    encoded[i + 1] = (encoded[i + 1] & reg) | dw;
                    bidx += 4;
                    return 1 + scoreSlow(encoded, bidx, i);
                }
            }
        }
        return 0;
    }


    /**
     * setup misc
     */
    public void setup(long seed, String[] board, int colors) {
        _start_time = System.currentTimeMillis();
        _end_time = _start_time + _xtime * 29 / 30;
        C = colors;
        N = board.length;
        WANT_COLUMN[0] = true;
        WANT_COLUMN[1 | (1 << 3)] = true;
        WANT_COLUMN[2 | (2 << 3)] = true;
        WANT_COLUMN[3 | (3 << 3)] = true;
        WANT_COLUMN[4 | (4 << 3)] = true;
        WANT_COLUMN[5 | (5 << 3)] = true;
        BUFFER[0] = (int)(seed % C);
        BUFFER_HEAD = seed;
        for (int i = 1; i < BUFFER.length; i++) {
            BUFFER_HEAD = (BUFFER_HEAD * 48271) % (2147483647L);
            BUFFER[i] = (int)(BUFFER_HEAD % C);
        }
    }

    /**
     * main logic
     *
     * @param colors
     * @param board
     * @param startSeed
     * @return
     */
    public int[] playIt(int colors, String[] board, int startSeed) {
        setup(startSeed, board, colors);

        for (String b : board) {
            debug(b);
        }
        for (int i = 0 ; i < 10 ; i++) {
            debug(BUFFER[i]);
        }


        int[] ret = beamMover(new State(encode(board), 0, 0));
        int[] ans = new int[30000];
        Arrays.fill(ans, 1);
        for (int i = 0; i < Math.min(ans.length, ret.length); i++) {
            ans[i] = ret[i];
        }
        debug("time", System.currentTimeMillis() - _start_time);
        debug("rtime", _recorder);
        return ans;
    }

    /**
     * select best state from current beam
     *
     * @param beam
     * @return
     */
    public State selectBestState(State[][] beam) {
        int score = -1;
        State best = null;
        for (int t = 0 ; t < beam.length ; t++) {
            for (int l = 0 ; l < beam[0].length ; l++) {
                if (beam[t][l] != null) {
                    if (score < beam[t][l].score) {
                        score = beam[t][l].score;
                        best = beam[t][l];
                    }
                }
            }
        }
        return best;
    }


    /**
     * generate final operations with specific state
     *
     * @param pseudoParent
     * @param bestState
     * @return
     */
    public int[] finalizeWithState(State pseudoParent, State bestState) {
        if (bestState == null) {
            int[] ret = new int[_maxTurn*3];
            Arrays.fill(ret, 1);
            return ret;
        }
        List<Integer> revop = new ArrayList<Integer>();
        while (bestState.parent != pseudoParent) {
            Collections.reverse(bestState.operations);
            revop.addAll(bestState.operations);
            bestState = bestState.parent;
        }
        Collections.reverse(revop);
        int[] ret = new int[revop.size()];
        for (int i = 0 ; i < ret.length ; i++) {
            ret[i] = revop.get(i);
        }
        return ret;
    }

    /**
     * beam search
     *
     * @param initial
     * @return
     */
    public int[] beamMover(State initial) {
    	final int ROT = 16;
    	final int BW = ((int)(Math.sqrt(900000.0/((N-1)*(N-1)*(2*C+8))))+1);
    	debug("BW="+BW);
        State pseudoParent = new State(null, -1, -1);
        pseudoParent.subScore = -1;

        State now = process(initial, -1, -1, -1);
        now.parent = pseudoParent;
        now.operations = new ArrayList<Integer>();
        now.turn = 0;
        int[] bsidx = new int[ROT];
        State[][] beam = new State[ROT][BW];
        beam[0][bsidx[0]++] = now;

        int[][][] sum = new int[C][N+1][N+1];
        for (int T = 0 ; T < _maxTurn ; T++) {
            int fT = T % ROT;

            // timeout
            if (System.currentTimeMillis() > _end_time) {
                return finalizeWithState(pseudoParent, selectBestState(beam));
            }

            // otherwise
            for (int l = 0 ; l < BW ; l++) {
            	if (beam[fT][l] == null) {
            		break;
            	}
                now = beam[fT][l];

                int[] nxcol = new int[C];
                int bidx = now.score<<2;
                for (int i = 0 ; i < 1 ; i++) {
                    nxcol[(int)BUFFER[bidx]]++;
                    nxcol[(int)BUFFER[bidx+1]]++;
                    nxcol[(int)BUFFER[bidx+2]]++;
                    nxcol[(int)BUFFER[bidx+3]]++;
                    int c =  (int)BUFFER[bidx+3];
                    if (nxcol[c] == 4) {
                        nxcol[c] = 0;
                        bidx += 4;
                        i--;
                    }
                }
                int nextBidx = bidx;

                for (int i = 0 ; i < N ; i++) {
                    for (int j = 0 ; j < N ; j++) {
                        int co = (int)((now.board[i] >> (3 * j)) & 7);
                        for (int c = 0 ; c < C ; c++) {
                            sum[c][i+1][j+1] = sum[c][i+1][j] + sum[c][i][j+1] - sum[c][i][j];
                            if (co == c) {
                                sum[c][i+1][j+1] += 1;
                            }
                        }
                    }
                }

                int subScore = 0;
                for (int i = 0 ; i < N-1 ; i++) {
                    for (int j = 0 ; j < N-1 ; j++) {
                        int[] co = new int[C];
                        int max = 0;
                        for (int q = 0 ; q < 4 ; q++) {
                            int c = (int)((now.board[i+QY[q]] >> (3 * (j+QX[q]))) & 7);
                            co[c]++;
                            max = Math.max(max, co[c]);
                        }
                        Arrays.sort(co);

                        int add = 0;
                        if (max == 2) {
                            add += 2;
                            if (co[C-2] == 2) {
                                add += 2;
                            }
                        } else if (max == 3) {
                            add += 6;
                        }
                        subScore += add;
                    }
                }
                now.subScore = subScore;

                int candidates = 0;
                int[] scores = new int[256];
                for (int i = 0; i < N - 1; i++) {
                    for (int j = 0; j < N - 1; j++) {
                        int max = 0;
                        int[] colors = new int[C];
                        for (int q = 0; q < 4; q++) {
                            int ti = i + QY[q];
                            int tj = j + QX[q];
                            int c = (int)((now.board[ti] >> (3 * tj)) & 7);
                            colors[c]++;
                            max = Math.max(max, colors[c]);
                        }
                        if (max <= 1) {
                            continue;
                        }

                        int mul = 0;
                        for (int q = 0 ; q < 4 ; q++) {
                            int ei = i + QY[q];
                            int ej = j + QX[q];
                            int c = (int)BUFFER[nextBidx+q];
                            int del = (sum[c][i+2][j+2] - sum[c][i][j+2] - sum[c][i+2][j] + sum[c][i][j]);
                            int fi = Math.max(0, ei-1);
                            int fj = Math.max(0, ej-1);
                            int ti = Math.min(ei+2, N);
                            int tj = Math.min(ej+2, N);
                            int add = (sum[c][ti][tj] - sum[c][fi][tj] - sum[c][ti][fj] + sum[c][fi][fj]);
                            mul += (add - del);
                        }
                    	int sc = mul;
                    	scores[(i << 4) + j] = sc  << 16 | (i << 4) | j;
                    	candidates++;
                    }
                }
                Arrays.sort(scores);

                for (int d = 0; d < 36 ; d++) {
                    if (scores[255-d] == 0) {
                        break;
                    }
                    int pos = scores[255-d] & ((1<<16)-1);
                    int i = pos >> 4;
                    int j = pos & 15;
                    int[] colors = new int[C];
                    int max = 0;
                    for (int q = 0; q < 4; q++) {
                        int ti = i + QY[q];
                        int tj = j + QX[q];
                        int c = (int)((now.board[ti] >> (3 * tj)) & 7);
                        colors[c]++;
                        max = Math.max(max, colors[c]);
                    }
                    for (int col = 0; col < C; col++) {
                        if (colors[col] < max)  {
                            continue;
                        }
                        collectFour(i, j, col, now.board, now);
                        if (collectFourBestScore < 0) {
                            continue;
                        }
                        int tT = (fT + findPathOPIdx / 3) % ROT;
                        int ts = collectFourBestScore + now.score;
                        int minScore = 1<<16;
                        int minSubScore = 1<<16;
                        int minScoreIdx = -1;
                        boolean foundSameWorse = false;
                        for (int k = 0 ; k < BW ; k++) {
                        	if (beam[tT][k] == null) {
                        		break;
                        	}
                        	if (beam[tT][k].score <= ts) {
                            	boolean isSame = true;
	                        	for (int f = 0 ; f < N ; f++) {
	                        		if (beam[tT][k].board[f] != collectFourBestBoard[f]) {
	                        			isSame = false;
	                        			break;
	                        		}
	                        	}
	                        	if (isSame) {
	                        		foundSameWorse = true;
	                        		minScoreIdx = k;
	                        	}
                        	}
                        }
                        if (!foundSameWorse) {
                            for (int k = 0 ; k < BW ; k++) {
                            	if (beam[tT][k] == null) {
                            		minScoreIdx = k;
                            		break;
                            	} else {
                            	    int DSi = (ts - beam[tT][k].score);
                            	    int DSSi = (subScore - beam[tT][k].subScore);
                            	    int TH = 1000*C/N/N;
                            	    if (T + _maxTurn / 20 >= _maxTurn) {
                            	        TH = 10000;
                            	    }
                            	    if (DSi > 0 || ((DSSi + DSi * TH) > 0)) {
                                        int _DSi = (minScore - beam[tT][k].score);
                                        int _DSSi = (minSubScore - beam[tT][k].subScore);
                                        if (_DSi > 0 || ((_DSSi + _DSi * TH) > 0)) {
                                            minScore = beam[tT][k].score;
                                            minSubScore = beam[tT][k].subScore;
                                            minScoreIdx = k;
                                        }
                            	    }
                            	}
                            }
                        }
                        if (minScoreIdx >= 0) {
                        	List<Integer> ops = new ArrayList<Integer>();
                        	for (int o = 0 ; o < findPathOPIdx ; o++) {
                        		ops.add(findPathOP[o]);
                        	}
                        	beam[tT][minScoreIdx] = new State(collectFourBestBoard, ts, now.turn + findPathOPIdx / 3, ops, now, i, j);
                        	beam[tT][minScoreIdx].subScore = now.subScore;
                        }
                    }
                }
            }
            int best = 0;
            int bc = 0;
            for (int x = 0 ; x < BW ; x++) {
            	if (beam[fT][x] != null) {
            		best = Math.max(best, beam[fT][x].score);
            		bc++;
            	}
            }
            Arrays.fill(beam[fT], null);
            debug(T, bc, best);
        }
        return finalizeWithState(pseudoParent, selectBestState(beam));
    }

    /**
     * collect four same color in square shape
     *
     * @param i
     * @param j
     * @param col
     * @param brd
     * @return
     */
    int collectFourBestScore;
    long[] collectFourBestBoard;
    boolean[] fixed = new boolean[256];
    public void collectFour(int i, int j, int col, long[] brd, State state) {
        Arrays.fill(fixed, false);
    	findPathOPIdx = 0;
    	collectFourBestScore = -1;
    	collectFourBestBoard = null;
        int[] prm = new int[4];
        int ph = 0;
        int pt = 3;
        for (int q = 0; q < 4; q++) {
            int fi = i + QY[q];
            int fj = j + QX[q];
            if (((brd[fi] >> (3 * fj)) & 7) == col) {
            	prm[ph++] = q;
            } else {
            	prm[pt--] = q;
            }
        }

        boolean isGood = true;
        long[] cpy = brd.clone();
        for (int q = 0; q < 4; q++) {
            int fi = i + QY[prm[q]];
            int fj = j + QX[prm[q]];
            boolean isok = findPath(fi, fj, col, cpy, fixed);
            if (!isok) {
                isGood = false;
                break;
            }
            fixed[(fi<<4)+fj] = true;
        }
        if (isGood) {
            long[] traceBoard = state.board.clone();
            int score = state.score;
            for (int o = 0 ; o < findPathOPIdx ; o += 3) {
                int fi = findPathOP[o];
                int fj = findPathOP[o+1];
                int d = findPathOP[o+2];
                swap(traceBoard, fi, fj, d);
                score += fastProcess(score, traceBoard, fi, fj, d);
            }
            collectFourBestScore = score - state.score;
            collectFourBestBoard = traceBoard;
        }
    }

    int[] findPathDp_prev = new int[256];
    int[] findPathDp = new int[256];
    int findPathScore = 0;
    int[] findPathOP = new int[32];
    int findPathOPIdx = 0;
    public boolean findPath(int fi, int fj, int col, long[] brd, boolean[] fixed) {
    	// same
    	if (((brd[fi] >> (3 * fj)) & 7) == col) {
    		return true;
    	}

    	//
    	//  1
    	// 1*1
    	//  1
    	//
    	for (int d = 0 ; d < 4 ; d++) {
    		int ti = fi + DY[d];
    		int tj = fj + DX[d];
    		if (ti < 0 || tj < 0 || ti >= N || tj >= N || fixed[(ti<<4)+tj]) {
                continue;
            }
    		if (((brd[ti] >> (3 * tj)) & 7) == col) {
    			findPathOP[findPathOPIdx++] = fi;
    			findPathOP[findPathOPIdx++] = fj;
    			findPathOP[findPathOPIdx++] = d;
                swap(brd, fi, fj, d);
                return true;
    		}
    	}

    	//
    	// 2-2
    	// -*-
    	// 2-2
    	//
    	for (int d = 4 ; d < 8 ; d++) {
    		int ti = fi + DY[d];
    		int tj = fj + DX[d];
    		if (ti < 0 || tj < 0 || ti >= N || tj >= N || fixed[(ti<<4)+tj]) {
                continue;
            }
    		if (((brd[ti] >> (3 * tj)) & 7) == col) {
    			// which way to go?
    		    //  y@
    		    //  *x
    			int codex = (fi<<4)+tj;
    			int codey = (ti<<4)+fj;
    			if (fixed[codex] && fixed[codey]) {
    				continue;
    			}
                int dx = ((d-4)/2)*2+1;
                int dy = (d%2)*2;
    			boolean goX = false;
    			if (fixed[codey]) {
    				// x is okay
    			    // -@
    			    // **
    				goX = true;
    			} else if (fixed[codex]) {
    				// y is okay
                    // *@
                    // *-
    			} else {
    			    // both is okay
    			    //  ??
    			    // y@?
    			    // *x
    			    goX = true;
    			}
    			if (goX) {
        			findPathOP[findPathOPIdx++] = fi;
        			findPathOP[findPathOPIdx++] = tj;
        			findPathOP[findPathOPIdx++] = dy;
                    swap(brd, fi, tj, dy);
        			findPathOP[findPathOPIdx++] = fi;
        			findPathOP[findPathOPIdx++] = fj;
        			findPathOP[findPathOPIdx++] = dx;
                    swap(brd, fi, fj, dx);
    			} else {
    				findPathOP[findPathOPIdx++] = ti;
        			findPathOP[findPathOPIdx++] = fj;
        			findPathOP[findPathOPIdx++] = dx;
                    swap(brd, ti, fj, dx);
        			findPathOP[findPathOPIdx++] = fi;
        			findPathOP[findPathOPIdx++] = fj;
        			findPathOP[findPathOPIdx++] = dy;
                    swap(brd, fi, fj, dy);
    			}
                return true;
    		}
    	}


    	//  3
    	// ---
    	//3-*-3
    	// ---
    	//  3
    	for (int d = 8 ; d < 12 ; d++) {
    		int ti = fi + DY[d];
    		int tj = fj + DX[d];
    		if (ti < 0 || tj < 0 || ti >= N || tj >= N || fixed[(ti<<4)+tj]) {
                continue;
            }
    		if (((brd[ti] >> (3 * tj)) & 7) == col) {
    			int ci = (fi+ti)/2;
    			int cj = (fj+tj)/2;
    			if (fixed[(ci<<4)+cj]) {
    				continue;
    			}
    			findPathOP[findPathOPIdx++] = ci;
    			findPathOP[findPathOPIdx++] = cj;
    			findPathOP[findPathOPIdx++] = d-8;
                swap(brd, ci, cj, d-8);
    			findPathOP[findPathOPIdx++] = fi;
    			findPathOP[findPathOPIdx++] = fj;
    			findPathOP[findPathOPIdx++] = d-8;
                swap(brd, fi, fj, d-8);
                return true;
    		}
    	}


        return false;
    }

    public double fastEvaluate(State state, int i, int j, int d) {
        return 0.0d;
    }

    // ============================================================================================================

    static class State {
        long[] board;
        int score;
        int turn;
        List<Integer> operations;
        State parent;
        int px;
        int py;
        int subScore;

        State(long[] b, int s, int t, List<Integer> op, State p, int x, int y) {
            board = b;
            score = s;
            turn = t;
            operations = op;
            parent = p;
            px = x;
            py = y;
        }

        State(long[] b, int s, int t) {
            board = b;
            score = s;
            turn = t;
            px = -10;
            py = -10;
        }
    }

    static class Move {
        int i;
        int j;
        int d;

        Move(int _i, int _j, int _d) {
            i = _i;
            j = _j;
            d = _d;
        }
    }

    // ============================================================================================================

    /**
     * main (for offline tester)
     *
     * @param args
     * @throws NumberFormatException
     * @throws IOException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int colors = Integer.valueOf(reader.readLine());
        int N = Integer.valueOf(reader.readLine());
        String[] board = new String[N];
        for (int i = 0; i < N; i++) {
            board[i] = reader.readLine();
        }
        int startSeed = Integer.valueOf(reader.readLine());

        SquareRemover sr = new SquareRemover();
        if (args.length >= 1) {
            _xtime = Long.valueOf(args[0]);
        }
        if (args.length >= 2) {
            _maxTurn = Integer.valueOf(args[1]);
        }
        if (args.length >= 3) {
            _debug = true;
        }
        int[] out = sr.playIt(colors, board, startSeed);
        for (int t = 0; t < out.length; t++) {
            System.out.println(out[t]);
        }
        System.out.flush();
    }

    /**
     * determine whether int[] a == int[] b
     *
     * @param a
     * @param b
     * @return
     */
    public static boolean equals(int[] a, int[] b) {
        int len = a.length;
        for (int i = 0; i < len; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * make copy of long[][] array
     *
     * @param a
     * @return
     */
    public static long[][] clone2(long[][] a) {
        int len = a.length;
        long[][] b = new long[len][];
        for (int i = 0; i < len; i++) {
            b[i] = a[i].clone();
        }
        return b;
    }

    /**
     * make copy of boolean[][] array
     *
     * @param a
     * @return
     */
    public static boolean[][] clone2(boolean[][] a) {
        int len = a.length;
        boolean[][] b = new boolean[len][];
        for (int i = 0; i < len; i++) {
            b[i] = a[i].clone();
        }
        return b;
    }

    /**
     * make copy of int[][] array
     *
     * @param a
     * @return
     */
    public static int[][] clone2(int[][] a) {
        int len = a.length;
        int[][] b = new int[len][];
        for (int i = 0; i < len; i++) {
            b[i] = a[i].clone();
        }
        return b;
    }

    /**
     * print debug
     *
     * @param os
     */
    public static void debug(Object... os) {
        if (_debug) {
            System.err.println(Arrays.deepToString(os));
        }
    }

    /**
     * benchmark
     *
     * @return
     */
    long bench(int rep) {
        long hoge = 0;
        for (int i = 0; i < rep; i++) {
            hoge *= 1000000009;
            hoge %= Integer.MAX_VALUE;
        }
        return hoge;
    }

    /**
     * permutation
     *
     * @param num
     * @return
     */
    public static boolean next_permutation(int[] num) {
        int len = num.length;
        int x = len - 2;
        while (x >= 0 && num[x] >= num[x + 1]) {
            x--;
        }
        if (x == -1)
            return false;

        int y = len - 1;
        while (y > x && num[y] <= num[x]) {
            y--;
        }
        int tmp = num[x];
        num[x] = num[y];
        num[y] = tmp;
        java.util.Arrays.sort(num, x + 1, len);
        return true;
    }


    public static void ___recordIt(String name) {
    	if (_record_timer.containsKey(name)) {
    		long time = System.currentTimeMillis() - _record_timer.get(name);
    		long ttime = (_recorder.containsKey(name)) ? _recorder.get(name) + time : time;
    		_recorder.put(name, ttime);
    		_record_timer.remove(name);
    	} else {
    		_record_timer.put(name, System.currentTimeMillis());
    	}
    }

}
