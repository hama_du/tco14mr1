

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class SquareRemoverTest {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	    SquareRemover._debug = true;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(timeout=1000)
	public void testBench() {
		SquareRemover sr = new SquareRemover();
		assertTrue("should compute in time", sr.bench(10000000) >= 0);
		assertTrue("should compute in time", sr.bench(100000000) >= 0);
	}
	
	
	@Test(timeout=10000)
	@Ignore
	public void testReturnValue() {
		SquareRemover sr = new SquareRemover();
		int[] ret = sr.playIt(6, buildRandomBoard(16, 6), 4);
		assertEquals("return length", 10000 * 3, ret.length);
	}
	
	@Test(timeout=2000)
	@Ignore
	public void testShortRun() {
		SquareRemover sr = new SquareRemover();
		SquareRemover._maxTurn = 2000;
		int[] ret = sr.playIt(6, buildRandomBoard(16, 4), 4);
		assertEquals("return length", 2000 * 3, ret.length);
	}
	
	@Test
	public void testSetup() {
		SquareRemover sr = new SquareRemover();
		sr.setup(1000, buildBoard(16), 6);
		assertTrue(SquareRemover.WANT_COLUMN[0]);
		assertTrue(SquareRemover.WANT_COLUMN[45]);
		assertEquals(SquareRemover.N, 16);
		assertEquals(SquareRemover.C, 6);
		assertEquals(SquareRemover.BUFFER[0], 1000 % 6);
		long b = 1000;
		for (int i = 1 ; i < 100 ; i++) {
			b = (b * 48271) % Integer.MAX_VALUE;
			assertEquals(SquareRemover.BUFFER[i], b % 6);
		}
 		
	}
	
	

	@Test
	public void testEncode() {
		String[] board = {
			"0000",			
			"0123",
			"2345",
			"5455",			
		};
		long[] expected = {
			0L,
			0|(1<<3)|(2<<6)|(3<<9),
			2|(3<<3)|(4<<6)|(5<<9),
			5|(4<<3)|(5<<6)|(5<<9),
		};
		SquareRemover sr = new SquareRemover();
		long[] ret = SquareRemover.encode(board);
		assertArrayEquals("encode correctly", expected, ret);
		
		String[] board2 = buildBoard(12);
		board2[0] = "555555555555";
		long[] ret2 = SquareRemover.encode(board2);
		Assert.assertTrue("encode correctly", ret2[0] > 1L<<34);
	}
	
	@Test
	public void testScoreSlow() {
		String[] board = {
			"0000",			
			"0000",
			"0110",
			"0110",			
		};
		SquareRemover sr = new SquareRemover();
		sr.setup(1, board, 4);
		long[] encoded = SquareRemover.encode(board);
		int ret = sr.scoreSlow(encoded, 0);
        assertEquals("count score correctly", 3, ret);
		String[] board2 = {
			"1311",			
			"2231",
			"0130",
			"0330",			
		};
		assertArrayEquals("change board correctly", SquareRemover.encode(board2), encoded);
	}
	
	
	@Test(timeout=500)
	public void testScoreFast() {
		String[] board = {
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
			"1234223412341234",
			"1234333412341234",
			"1234223412341234",
			"1234003412341234",
			"1234003412341234",
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
		};
		String[] expected = {
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
			"1234330412341234",
			"1234121412341234",
			"1234323412341234",
			"1234233412341234",
			"1234013412341234",
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
			"1234123412341234",
		};
		
		SquareRemover sr = new SquareRemover();
		sr.setup(2, board, 4);
		int sc = 0;
		long[] encoded = SquareRemover.encode(board);
		for (int i = 0 ; i < board.length ; i++) {
			for (int j = 0 ; j < board.length ; j++) {
				// sc += sr.scoreFast(encoded, 0, i, j);
			}
		}
		assertArrayEquals("change board correctly", SquareRemover.encode(expected), encoded);
        assertEquals("count score correctly", 6, sc);
	}
	
	
	@Test
	public void testSwap() {
	    String[] board = {
            "430",
            "215",
            "432"
	    };
        String[] down = {
            "230",
            "415",
            "432"
        };
        String[] right = {
            "230",
            "145",
            "432"
        };
        String[] up = {
            "240",
            "135",
            "432"
        };
        String[] left = {
            "420",
            "135",
            "432"
        };
	    long[] code = SquareRemover.encode(board);
	    SquareRemover.swap(code, 0, 0, SquareRemover.DIR_DOWN);
	    assertArrayEquals("change board correctly", SquareRemover.encode(down), code);
        
        SquareRemover.swap(code, 1, 0, SquareRemover.DIR_RIGHT);
        assertArrayEquals("change board correctly", SquareRemover.encode(right), code);

        SquareRemover.swap(code, 1, 1, SquareRemover.DIR_UP);
        assertArrayEquals("change board correctly", SquareRemover.encode(up), code);

        SquareRemover.swap(code, 0, 1, SquareRemover.DIR_LEFT);
        assertArrayEquals("change board correctly", SquareRemover.encode(left), code);
	}
	
	@Test
	public void testCollectFour() {
		String[] board = {
				"12345",
				"12345",
				"12345",
				"12345",			
				"12345",			
			};
			SquareRemover sr = new SquareRemover();
			sr.setup(1, board, 6);
			long[] encoded = SquareRemover.encode(board);
			//	assertEquals("can collect", 4, sr.collectFour(1, 1, 2, encoded.clone()).size() / 3);
	}
	
	
	public String[] buildBoard(int n) {
		String[] ret = new String[n];
		for (int i = 0 ; i < n ; i++) {
			ret[i] = "";
			for (int j = 0 ; j < n ; j++) {
				ret[i] += "0";
			}
		}
		return ret;
	}
	
	public String[] buildRandomBoard(int n, int cn) {
		String[] ret = new String[n];
		for (int i = 0 ; i < n ; i++) {
			ret[i] = "";
			for (int j = 0 ; j < n ; j++) {
				int c = (int)(Math.random() * cn);
				ret[i] += c;
			}
		}
		return ret;
	}
	
	public String[] decode(long[] code) {
	    int n = code.length;
	    String[] ret = new String[n];
        for (int i = 0 ; i < n ; i++) {
            ret[i] = "";
            for (int j = 0 ; j < n ; j++) {
                long c = (code[i]>>(j*3))&7;
                ret[i] += (char)('0' + c);
            }
        }
        return ret;
	}
	
	public static void debug(Object... o) {
	    System.err.println(Arrays.deepToString(o));
	}
}
