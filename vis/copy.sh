#!/bin/sh

from=$1
to=$2
program=$3
limit=$4
datafile=out/$5
if [ $# -lt 1 ]; then
  from=0
  to=100
  program="SquareRemover"
  limit=20000
  datafile="out/`date +"%Y%m%d%I%M.txt"`"
fi

# copy *.java
cp ../src/$program.java .
javac $program.java

# copy *.cpp
cp ../src/$program.cpp .
g++ -o square -O2 -pipe -mmmx -msse -msse2 -msse3 $program.cpp

echo "java -jar Visualizer.jar -exec \"java -Xms512m -Xmx512m $program $limit 10000\" -novis -seed 1"
echo "java -jar Visualizer.jar -exec \"./square $limit 10000\" -novis -seed 1"

