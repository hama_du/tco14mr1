#!/bin/sh

size=$2
program="SquareRemover"
limit=$3
turn=$4
if [ $# -lt 2 ]; then
  size=10
  program="SquareRemover"
  limit=3000
  turn=1000
fi

cp ../src/$program.java .
javac $program.java

dir=out/nc/nc_"`date +"%Y%m%d%H%M_$1"`"/"$turn"
summaryfile="$dir"/summary.txt
if [ $# -gt 0 ] && [ $1 != "none" ] ; then
  mkdir -p $dir
  touch $summaryfile
fi
 
c=3
while [ $c -lt 6 ]; do
  c=`expr $c + 1`
  n=7
  while [ $n -lt 16 ]; do
    n=`expr $n + 1`
    i=`expr $c \* 1000 + $n \* 100 - 2800`
    to=`expr $i + $size`
    datafile="$dir"/c"$c"_n"$n".txt
    if [ $# -gt 0 ] && [ $1 != "none" ] ; then
      touch $datafile
    fi
    all=0
    while [ $i -lt $to ]; do
      result=`java -jar Visualizer.jar -exec "java -Xms512m -Xmx512m $program $limit $turn" -novis -seed $i`
      if [ $# -lt 1 ] || [ $1 = "none" ]; then
        echo "$i,$result"
      else
        echo "$i,$result" >> $datafile
      fi
      all=`expr $all + $result`
      i=`expr $i + 1`
    done;
    if [ $# -lt 1 ] || [ $1 = "none" ]; then
      echo "all($c,$n)=$all"
    else
      echo "all($c,$n)=$all" >> $summaryfile
    fi
  done;
done;

