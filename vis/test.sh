#!/bin/sh

from=$2
to=$3
program="SquareRemover"
limit=$4
turn=$5
if [ $# -lt 2 ]; then
  from=0
  to=100
  program="SquareRemover"
  limit=100000
  turn=2000
fi

cp ../src/$program.java .
javac $program.java
mkdir -p out/"$from"-"$to"/"$turn"
datafile=out/"$from"-"$to"/"$turn"/"`date +"%Y%m%d%H%M_$1.txt"`"
if [ $# -gt 0 ] && [ $1 != "none" ] ; then
  touch $datafile
fi

i=$from
all=0
while [ $i -lt $to ]; do
	i=`expr $i + 1`
	result=`java -jar Visualizer.jar -exec "java -Xms512m -Xmx512m $program $limit $turn" -novis -seed $i`
  if [ $# -lt 1 ] || [ $1 = "none" ]; then
    echo "$i,$result"
  else
	  echo "$i,$result" >> $datafile
  fi
  all=`expr $all + $result`
done;
echo $all

